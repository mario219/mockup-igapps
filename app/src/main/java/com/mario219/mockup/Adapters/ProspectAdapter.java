package com.mario219.mockup.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mario219.mockup.R;
import com.mario219.mockup.app.RealmPref;
import com.mario219.mockup.persistence.RealmController;
import com.mario219.mockup.realmModel.ProspectRealm;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by marioalejndro on 25/08/16.
 */
public class ProspectAdapter extends RealmRecyclerViewAdapter<ProspectRealm> {

    final Context context;
    private Realm realm;
    private LayoutInflater inflater;

    public ProspectAdapter(Context context) {

        this.context = context;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prospect_item, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        realm = RealmController.getInstance().getRealm();

        // get the article
        final ProspectRealm prospectRealm = getItem(position);
        // cast the generic view holder to our specific one
        final CardViewHolder holder = (CardViewHolder) viewHolder;

        // set the title and the snippet
        holder.name.setText(prospectRealm.getName());
        holder.surname.setText(prospectRealm.getSurname());
        holder.document.setText(prospectRealm.getSchProspectIdentification());
        holder.cellphone.setText(prospectRealm.getTelephone());


        //remove single match from realm
        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                RealmResults<ProspectRealm> results = realm.where(ProspectRealm.class).findAll();

                // Get the book title to show it in toast message
                ProspectRealm b = results.get(position);
                String name = b.getName();

                // All changes to data must happen in a transaction
                realm.beginTransaction();

                // remove single match
                results.remove(position);
                realm.commitTransaction();

                if (results.size() == 0) {
                    RealmPref.with(context).setPreLoad(false);
                }

                notifyDataSetChanged();

                Toast.makeText(context, name + " is removed from Realm", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        //update single match from realm
        holder.card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.edit_prospect, null);
                final EditText editName = (EditText) content.findViewById(R.id.tfName);
                final EditText editSurname = (EditText) content.findViewById(R.id.tfSurname);
                final EditText editDocument = (EditText) content.findViewById(R.id.tfDocument);
                final EditText editCellphone = (EditText) content.findViewById(R.id.tfCellphone);

                editName.setText(prospectRealm.getName());
                editSurname.setText(prospectRealm.getSurname());
                editDocument.setText(prospectRealm.getSchProspectIdentification());
                editCellphone.setText(prospectRealm.getSchProspectIdentification());

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(content)
                        .setTitle("Editar prospecto")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                RealmResults<ProspectRealm> results = realm.where(ProspectRealm.class).findAll();

                                realm.beginTransaction();
                                results.get(position).setName(editName.getText().toString());
                                results.get(position).setSurname(editSurname.getText().toString());
                                results.get(position).setSchProspectIdentification(editDocument.getText().toString());
                                results.get(position).setTelephone(editCellphone.getText().toString());

                                realm.commitTransaction();

                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {

        if (getRealmAdapter() != null) {
            return getRealmAdapter().getCount();
        }
        return 0;
    }


    public static class CardViewHolder extends RecyclerView.ViewHolder {

        public CardView card;
        public TextView name;
        public TextView surname;
        public TextView document;
        public TextView cellphone;

        public CardViewHolder(View itemView) {

            super(itemView);

            card = (CardView) itemView.findViewById(R.id.card_prospect);
            name = (TextView) itemView.findViewById(R.id.name);
            surname = (TextView) itemView.findViewById(R.id.surname);
            document = (TextView) itemView.findViewById(R.id.document);
            cellphone = (TextView) itemView.findViewById(R.id.cellphone);
        }
    }
}
