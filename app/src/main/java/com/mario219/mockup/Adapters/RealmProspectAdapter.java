package com.mario219.mockup.Adapters;

import android.content.Context;
import com.mario219.mockup.realmModel.ProspectRealm;
import io.realm.RealmResults;

/**
 * Created by marioalejndro on 25/08/16.
 */
public class RealmProspectAdapter extends RealmModelAdapter<ProspectRealm> {
    public RealmProspectAdapter(Context context, RealmResults<ProspectRealm> realmResults, boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
    }
}
