package com.mario219.mockup.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mario219 on 25/08/16.
 */

public class User {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("authToken")
    @Expose
    private String authToken;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("zone")
    @Expose
    private Object zone;

    /**
     *
     * @return
     *     The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     *
     * @param success
     *     The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     *
     * @return
     *     The authToken
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     *
     * @param authToken
     *     The authToken
     */
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    /**
     *
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     *     The zone
     */
    public Object getZone() {
        return zone;
    }

    /**
     *
     * @param zone
     *     The zone
     */
    public void setZone(Object zone) {
        this.zone = zone;
    }

}
