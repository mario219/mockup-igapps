package com.mario219.mockup.persistence;

/**
 * Created by mario219 on 25/08/16.
 */

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;
import com.mario219.mockup.realmModel.ProspectRealm;
import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresca la instancia Realm
    public void refresh() {

        realm.refresh();
    }

    //Elimina todos los prospectos
    public void clearAll() {

        realm.beginTransaction();
        realm.clear(ProspectRealm.class);
        realm.commitTransaction();
    }

    //Encuentra todos los prospectos
    public RealmResults<ProspectRealm> getAllProspects() {

        return realm.where(ProspectRealm.class).findAll();
    }

    //Encuentra un prospecto por Id
    public ProspectRealm getProspectById(String id) {

        return realm.where(ProspectRealm.class).equalTo("id", id).findFirst();
    }

    //Checa si no hay prospectos
    public boolean hasProspects() {

        return !realm.allObjects(ProspectRealm.class).isEmpty();
    }

    //query example
    public RealmResults<ProspectRealm> queryedProspects() {

        return realm.where(ProspectRealm.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}