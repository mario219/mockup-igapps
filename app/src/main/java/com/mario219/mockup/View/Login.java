package com.mario219.mockup.view;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.mario219.mockup.control.RestApi;
import com.mario219.mockup.app.SessionsPref;
import com.mario219.mockup.model.User;
import com.mario219.mockup.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Login extends AppCompatActivity {
    private static final String TAG = Login.class.getSimpleName();
    private final String url = "http://directotesting.igapps.co/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(SessionsPref.getUser(getApplicationContext()).equals("default")){
            Button btnMostrar = (Button) findViewById(R.id.btnMostrar);
            btnMostrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getReport();
                }
            });
        }else{
            Intent session = new Intent(this, MainMenu.class);
            startActivity(session);
            Login.this.finish();
        }

    }

    private void getReport() {

        final TextView email, password;
        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.spin_kit);
        DoubleBounce doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);
        progressBar.setVisibility(View.VISIBLE);
        email = (TextView) findViewById(R.id.tfUser);
        password = (TextView) findViewById(R.id.tfPass);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestApi service = retrofit.create(RestApi.class);

        Call<User> call = service.userLogin(email.getText().toString(), password.getText().toString());
        email.setText("");
        password.setText("");
        call.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                try {
                    SessionsPref.login(getApplicationContext(), response.body().getEmail(), response.body().getAuthToken());
                    Intent session = new Intent(getApplicationContext(), MainMenu.class);
                    startActivity(session);
                    Login.this.finish();
                    Toast.makeText(getApplicationContext(), response.body().getAuthToken(), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Error, email o password incorrectos", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.i(TAG, "Error, email o password incorrectos");
            }
        });

    }
}