package com.mario219.mockup.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.mario219.mockup.R;
import com.mario219.mockup.app.RealmPref;
import com.mario219.mockup.Adapters.ProspectAdapter;
import com.mario219.mockup.Adapters.RealmProspectAdapter;
import com.mario219.mockup.app.SessionsPref;
import com.mario219.mockup.control.RestApi;
import com.mario219.mockup.model.Prospects;
import com.mario219.mockup.model.ProspectsGroup;
import com.mario219.mockup.realmModel.ProspectRealm;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainMenu.class.getSimpleName();
    private ProspectAdapter adapter;
    private Realm realm;
    private LayoutInflater inflater;
    private RecyclerView recycler;
    private final String url = "http://directotesting.igapps.co/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        recycler = (RecyclerView) findViewById(R.id.recycler);

        //get realm instance
     //   this.realm = RealmController.with(this).getRealm();

        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

     //   setupRecycler();
         getProspects();
        if (!RealmPref.with(this).getPreLoad()) {
            //setRealmData();
        }

        // refresh the realm instance
     //   RealmController.with(this).refresh();
        // get all persisted objects
        // create the helper adapter and notify data set changes
        // changes will be reflected automatically
    //    setRealmAdapter(RealmController.with(this).getAllProspects());

        //Toast.makeText(this, "Press card item for edit, long press to remove item", Toast.LENGTH_LONG).show();

        //add new item
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionsPref.logout(getApplicationContext());
                MainMenu.this.finish();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void setRealmAdapter(RealmResults<ProspectRealm> prospects) {

        RealmProspectAdapter realmAdapter = new RealmProspectAdapter(this.getApplicationContext(), prospects, true);
        // Set the data and tell the RecyclerView to draw
        adapter.setRealmAdapter(realmAdapter);
        adapter.notifyDataSetChanged();
    }

    private void setupRecycler() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler.setHasFixedSize(true);

        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        // create an empty adapter and add it to the recycler view
        adapter = new ProspectAdapter(this);
        recycler.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getProspects(){

        GsonBuilder gsonBuilder = new GsonBuilder();
        //gsonBuilder.registerTypeAdapter(ProspectsGroup.class, new ProspectsDeserializer());
        Gson gson = gsonBuilder
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RestApi service2 = retrofit2.create(RestApi.class);
        Log.i(TAG, SessionsPref.getUser(getApplicationContext()));
        Call<List<Prospects>> call = service2.getProspects(SessionsPref.getUser(getApplicationContext()));
        call.enqueue(new Callback <List<Prospects>>() {

            @Override
            public void onResponse(Call<List<Prospects>> call, Response<List<Prospects>> response) {

                try {
                    Log.i(TAG, "holaaaaaaaa");
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<List<Prospects>> call, Throwable t) {
                Log.i(TAG, "No se pudo realizar la operacion: ");
                t.printStackTrace();
            }
        });

    }

   /* private class ProspectsDeserializer
            implements JsonDeserializer<ProspectsGroup> {

        @Override
        public ProspectsGroup deserialize(JsonElement json, Type type,
                                                 JsonDeserializationContext context) throws JsonParseException {

            JsonArray jArray = (JsonArray) json;

            ProspectsGroup prospectsGroup = new ProspectsGroup();

            for (int i=1; i<jArray.size(); i++) {
                JsonObject jObject = (JsonObject) jArray.get(i);
                int id = jObject.get("id").getAsInt();
                String name = jObject.get("name").getAsString();
                String surname = jObject.get("surname").getAsString();
                String cellphone = jObject.get("telephone").getAsString();
                String schProspectIdentification = jObject.get("schProspectIdentification").getAsString();
                int statusCd = jObject.get("statusCd").getAsInt();
                Prospects prospect = new Prospects();
                prospect.setId(String.valueOf(id));
                prospect.setName(name);
                prospect.setSurname(surname);
                prospect.setTelephone(cellphone);
                prospect.setSchProspectIdentification(schProspectIdentification);
                prospect.setStatusCd(statusCd);
                prospectsGroup.getResults().add(prospect);
            }

            return prospectsGroup;
        }


    }*/

}
