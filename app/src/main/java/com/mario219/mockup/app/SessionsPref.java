package com.mario219.mockup.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by mario219 on 25/08/16.
 */
public class SessionsPref {

    private static final String TAG = SessionsPref.class.getSimpleName();
    private static String PREF_NAME = "Session";

    public SessionsPref(){

    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String getUser(Context context) {
        return getPrefs(context).getString("token", "default");
    }

    public static void login(Context context, String user, String token) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString("user", user);
        editor.putString("token", token);
        editor.commit();
        Log.i(TAG, "user logged");
    }

    public static void logout(Context context){
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString("user", "default");
        editor.putString("token", "default");
        editor.commit();
        Log.i(TAG, "session terminated");
    }
}
