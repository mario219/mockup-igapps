package com.mario219.mockup.app;

/**
 * Created by mario219 on 25/08/16.
 */

import android.app.Application;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ApplicationSetup extends Application {

    @Override
    public void onCreate() {

        super.onCreate();
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

    }

}
