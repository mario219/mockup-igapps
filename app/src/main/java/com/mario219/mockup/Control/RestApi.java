package com.mario219.mockup.control;

import com.mario219.mockup.model.Prospects;
import com.mario219.mockup.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mario219 on 25/08/16.
 */

public interface RestApi {

    @GET("application/login/")
    Call<User> userLogin(@Query ("email") String email, @Query("password") String pass);


    @GET("sch/prospects.json/")
    Call<List<Prospects>> getProspects(@Header("authToken") String token);
}
